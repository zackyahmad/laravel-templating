<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyController extends Controller
{
    public function master(){
        return view('layouts.master');
    }
    public function welcome(){
        return view('pages.welcome');
    }
    
    public function index(){
        return view('pages.index');
    }
    public function regist(){
        return view('pages.register');
    }
    public function kirim(Request $request){
        $fnama =$request['fname'];
        $lnama =$request['lname'];
        $biodata = $request['bio'];
        $jenisKelamin = $request['jk'];
        return view('pages.welcome', ['fname' => $fnama, 'lname' => $lnama, 'jenisKelamin' => $jenisKelamin , 'bio' => $biodata]);
    }
}
