<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MyController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/',[MyController::class, 'master']);
Route::get('/index',[MyController::class, 'index']);
Route::get('/register',[MyController::class, 'regist']);
Route::post('/kirim',[MyController::class, 'kirim']);
Route::get('/welcome',[MyController::class, 'welcome']);
Route::get('/biodata', [MyController:: class, 'bio']);

Route::get('/data-table', function(){
    return view('pages.table');
});

Route::get('/index', function(){
    return view('pages.index');
});