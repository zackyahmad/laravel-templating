@extends('layouts.master')


@section('judul')
<h4>Halaman Form</h4>
@endsection

@section('content')
    <h1>Buat Account Baru</h1><br>
    <form action="/kirim" method="post">
        @csrf
        <h3>Sign Up Form</h3><br>
        <label>first name</label><br>
        <input type="text" name="fname"><br><br> 
        <label>last name</label><br>
        <input type="text" name="lname"><br><br>

        <label>gender</label><br>
        <input type="radio" name="jk" id="male" value="1"> Laki laki <br>
        <input type="radio" name="jk" id="female" value="2"> Perempuan
        <br>
        <label for="negara">Nationality</label><br>
        <select name="negara" id="negara">
            <option value="1">Indonesia</option>
            <option value="2">Malaysia</option>
            <option value="3">Inggris</option>
            <option value="4">Singapore</option>
        </select>
        <br><br>

        <label for="">Languange Spoken</label><br>
        <input type="checkbox" name="" id="bindo" value="">
        <label for="bindo">Bahasa Indonesia</label><br>
        <input type="checkbox" name="" id="english" value="">
        <label for="english">English</label><br>
        <input type="checkbox" name="" id="other" value="">
        <label for="other">Other</label><br><br>

        <label >bio</label><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="kirim">

    </form>
@endsection